package ru.t1.stepanishchev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.model.IWBS;
import ru.t1.stepanishchev.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    public ProjectDTO(@NotNull final String name, @NotNull final Status status, @Nullable final String description) {
        this.name = name;
        this.status = status;
        this.description = description;
    }

    public ProjectDTO(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        super(userId);
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}