package ru.t1.stepanishchev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

@Service
public interface IUserDTOService extends IAbstractDTOService<UserDTO> {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findOneByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    UserDTO lockUserByLogin(@Nullable String login);

    UserDTO unlockUserByLogin(@Nullable String login);

    void set(@NotNull Collection<UserDTO> users);

    void clear();

}