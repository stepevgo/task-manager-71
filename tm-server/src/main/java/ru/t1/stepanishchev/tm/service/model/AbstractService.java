package ru.t1.stepanishchev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanishchev.tm.api.service.model.IAbstractService;
import ru.t1.stepanishchev.tm.model.AbstractModel;
import ru.t1.stepanishchev.tm.repository.model.AbstractRepository;

import java.util.Collection;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel>
        implements IAbstractService<M> {

    @NotNull
    protected AbstractRepository<M> repository;

    @Transactional
    public void add(@NotNull final M model) {
        repository.save(model);
    }

    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        repository.saveAll(models);
    }

    @Transactional
    public void update(@NotNull final M model) {
        repository.save(model);
    }

    @Transactional
    public void remove(@NotNull final M model) {
        repository.delete(model);
    }

}