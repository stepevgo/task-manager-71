package ru.t1.stepanishchev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.Collection;
import java.util.List;

@Service
public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    String getSortType(@Nullable TaskSort sort);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<Task> findAll();

    @Nullable
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @NotNull
    Task removeById(
            @Nullable String userId,
            @Nullable String id
    );

    void set(@NotNull Collection<Task> tasks);

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}