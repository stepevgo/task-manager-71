package ru.t1.stepanischev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.stepanischev.tm.configuration.DataBaseConfiguration;
import ru.t1.stepanischev.tm.marker.UnitCategory;
import ru.t1.stepanischev.tm.model.TaskDTO;
import ru.t1.stepanischev.tm.service.TaskService;
import ru.t1.stepanischev.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("kek", "kek");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskService.add(task1);
        taskService.add(task2);
    }

    @After
    public void cleanTest() {
        taskService.clear();
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findOneByIdAndUserIdTest() {
        Assert.assertNotNull(taskService.findOneByIdAndUserId(task1.getId(), UserUtil.getUserId()));
        Assert.assertNotNull(taskService.findOneByIdAndUserId(task2.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserIdTest() {
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        tasks.add(task1);
        Assert.assertNotNull(taskService.findOneByIdAndUserId(task1.getId(), UserUtil.getUserId()));
        taskService.removeByIdAndUserId(task1.getId(), UserUtil.getUserId());
        Assert.assertNull(taskService.findOneByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByUserIdTest() {
        taskService.removeByUserId(task1, UserUtil.getUserId());
        taskService.removeByUserId(task2, UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

}