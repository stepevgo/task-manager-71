package ru.t1.stepanischev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.stepanischev.tm.configuration.DataBaseConfiguration;
import ru.t1.stepanischev.tm.marker.UnitCategory;
import ru.t1.stepanischev.tm.model.ProjectDTO;
import ru.t1.stepanischev.tm.service.ProjectService;
import ru.t1.stepanischev.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("kek", "kek");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectService.add(project1);
        projectService.add(project2);
    }

    @After
    public void cleanTest() {
        projectService.clear();
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findOneByIdAndUserIdTest() {
        Assert.assertNotNull(projectService.findOneByIdAndUserId(project1.getId(), UserUtil.getUserId()));
        Assert.assertNotNull(projectService.findOneByIdAndUserId(project2.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserIdTest() {
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        projects.add(project1);
        Assert.assertNotNull(projectService.findOneByIdAndUserId(project1.getId(), UserUtil.getUserId()));
        projectService.removeByIdAndUserId(project1.getId(), UserUtil.getUserId());
        Assert.assertNull(projectService.findOneByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByUserIdTest() {
        projectService.removeByUserId(project1, UserUtil.getUserId());
        projectService.removeByUserId(project2, UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

}
