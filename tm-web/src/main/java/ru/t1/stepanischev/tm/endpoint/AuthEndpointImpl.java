package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.stepanischev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.stepanischev.tm.api.repository.IUserRepository;
import ru.t1.stepanischev.tm.model.Result;
import ru.t1.stepanischev.tm.model.User;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.stepanischev.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpointImpl implements IAuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserRepository userRepository;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/profile")
    public User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userRepository.findByLogin(login);
    }

    @Override
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}