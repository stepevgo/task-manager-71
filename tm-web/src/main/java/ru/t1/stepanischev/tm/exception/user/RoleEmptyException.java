package ru.t1.stepanischev.tm.exception.user;

public class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}