package ru.t1.stepanischev.tm.exception.user;

import ru.t1.stepanischev.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(final String message) {
        super(message);
    }
    public AbstractUserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(final Throwable cause) {
        super(cause);
    }

    public  AbstractUserException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}