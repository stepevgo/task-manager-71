package ru.t1.stepanischev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.stepanischev.tm.enumerated.Status;
import ru.t1.stepanischev.tm.model.CustomUser;
import ru.t1.stepanischev.tm.model.TaskDTO;
import ru.t1.stepanischev.tm.service.ProjectService;
import ru.t1.stepanischev.tm.service.TaskService;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private TaskService taskService;

    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskService.addByUserId(new TaskDTO("New Task " + System.currentTimeMillis()), user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeByIdAndUserId(id, user.getUserId());
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDTO task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.addByUserId(task, user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        @Nullable final TaskDTO task = taskService.findOneByIdAndUserId(id, user.getUserId());
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

}