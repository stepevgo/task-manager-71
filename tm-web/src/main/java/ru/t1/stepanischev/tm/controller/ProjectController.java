package ru.t1.stepanischev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.stepanischev.tm.enumerated.Status;
import ru.t1.stepanischev.tm.model.CustomUser;
import ru.t1.stepanischev.tm.model.ProjectDTO;
import ru.t1.stepanischev.tm.service.ProjectService;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        projectService.addByUserId(new ProjectDTO("New Project " + System.currentTimeMillis()), user.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.removeByIdAndUserId(id, user.getUserId());
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("project") ProjectDTO project,
            BindingResult result
    ) {
        project.setUserId(user.getUserId());
        projectService.addByUserId(project, user.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        @Nullable final ProjectDTO project = projectService.findOneByIdAndUserId(id, user.getUserId());
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
