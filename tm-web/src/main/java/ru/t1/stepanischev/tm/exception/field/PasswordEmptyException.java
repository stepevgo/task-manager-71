package ru.t1.stepanischev.tm.exception.field;

public class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}