package ru.t1.stepanischev.tm.exception.user;

public class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

    public ExistsLoginException(String login) {
        super("Error! Login '" + login + "'already exists...");
    }

}