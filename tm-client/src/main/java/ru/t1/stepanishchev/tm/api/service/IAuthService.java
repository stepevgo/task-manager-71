package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.User;

import javax.management.relation.Role;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @Nullable
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    User check(@Nullable String login, @Nullable String password);

}